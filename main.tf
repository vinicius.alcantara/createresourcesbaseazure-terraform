terraform {
  required_version = ">=0.12"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "infra-devops" {
  name     = var.rsg
  location = var.location
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet
  resource_group_name = azurerm_resource_group.infra-devops.name
  location            = azurerm_resource_group.infra-devops.location
  address_space       = ["192.168.0.0/22"]
  # dns_servers         = ["192.168.0.4", "192.168.0.5"]
}

resource "azurerm_subnet" "subnet" {
  name                 = var.subnet
  resource_group_name  = azurerm_resource_group.infra-devops.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["192.168.0.0/24"]
  service_endpoints = [
    "Microsoft.AzureActiveDirectory",
    "Microsoft.AzureCosmosDB",
    "Microsoft.ContainerRegistry",
    "Microsoft.EventHub",
    "Microsoft.KeyVault",
    "Microsoft.ServiceBus",
    "Microsoft.Sql",
    "Microsoft.Storage",
    "Microsoft.Web"
  ]
}

resource "azurerm_network_security_group" "infra-devops" {
  name                = var.nsg
  resource_group_name = azurerm_resource_group.infra-devops.name
  location            = azurerm_resource_group.infra-devops.location
}

resource "azurerm_network_security_rule" "input-rules" {
  for_each                    = var.input_rules
  name                        = "input_port_${each.value}"
  priority                    = each.key
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = each.value
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.infra-devops.name
  network_security_group_name = azurerm_network_security_group.infra-devops.name
}

resource "azurerm_subnet_network_security_group_association" "nsg-association" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.infra-devops.id
}