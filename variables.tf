variable "location" {
  type        = string
  description = "Especifica a região"
  default     = "EastUS2"
}

variable "rsg" {
  type        = string
  description = "Especifica o nome do resource group"
  default     = "infra-devops"
}

variable "vnet" {
  type        = string
  description = "Define o nome da rede virtual privada"
  default     = "infra-devops"
}

variable "subnet" {
  type        = string
  description = "Define o nome da subnet"
  default     = "infra-devops"
}

variable "nsg" {
  type        = string
  description = "Especifica o mome do network security group"
  default     = "infra-devops"
}

variable "input_rules" {
  type = map(any)
  default = {
    101 = 80
    102 = 443
    103 = 22
    104 = 8080
  }
}

